package domain;

public class GruntyGeNs {
    private String nazwaGrNs;
    private double ge1;
    private double ge2;
    private double ge3;

    public GruntyGeNs() {
    }

    public GruntyGeNs(String nazwaGrNs, double ge1, double ge2, double ge3) {
        this.nazwaGrNs = nazwaGrNs;
        this.ge1 = ge1;
        this.ge2 = ge2;
        this.ge3 = ge3;
    }

    public String getNazwaGrNs() {
        return nazwaGrNs;
    }

    public void setNazwaGrNs(String nazwaGrNs) {
        this.nazwaGrNs = nazwaGrNs;
    }

    public double getGe1() {
        return ge1;
    }

    public void setGe1(double ge1) {
        this.ge1 = ge1;
    }

    public double getGe2() {
        return ge2;
    }

    public void setGe2(double ge2) {
        this.ge2 = ge2;
    }

    public double getGe3() {
        return ge3;
    }

    public void setGe3(double ge3) {
        this.ge3 = ge3;
    }

}
