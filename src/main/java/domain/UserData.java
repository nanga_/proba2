package domain;

public class UserData {
    public String nazwaWarstwy1;
    public double iw1;
    public double grw1;

    public UserData() {
    }

    public UserData(String nazwaWarstwy1, double iw1, double grw1) {
        this.nazwaWarstwy1 = nazwaWarstwy1;
        this.iw1 = iw1;
        this.grw1 = grw1;
    }

    public String getNazwaWarstwy1() {
        return nazwaWarstwy1;
    }

    public String setNazwaWarstwy1(String nazwaWarstwy1) {
        this.nazwaWarstwy1 = nazwaWarstwy1;
        return nazwaWarstwy1;

    }

    public double getIw1() {
        return iw1;
    }

    public void setIw1(double iw1) {
        this.iw1 = iw1;
    }

    public double getGrw1() {
        return grw1;
    }

    public void setGrw1(double grw1) {
        this.grw1 = grw1;
    }


}
