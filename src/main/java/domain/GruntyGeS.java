package domain;

public class GruntyGeS {
    private String nazwaGrS;
    double geS1;
    double geS2;
    double geS3;
    double geS4;

    public GruntyGeS(String nazwaGrS, double geS1, double geS2, double geS3, double geS4) {
        this.nazwaGrS = nazwaGrS;
        this.geS1 = geS1;
        this.geS2 = geS2;
        this.geS3 = geS3;
        this.geS4 = geS4;
    }

    public String getNazwaGrS() {
        return nazwaGrS;
    }

    public void setNazwaGrS(String nazwaGrS) {
        this.nazwaGrS = nazwaGrS;
    }

    public double getGeS1() {
        return geS1;
    }

    public void setGeS1(double geS1) {
        this.geS1 = geS1;
    }

    public double getGeS2() {
        return geS2;
    }

    public void setGeS2(double geS2) {
        this.geS2 = geS2;
    }

    public double getGeS3() {
        return geS3;
    }

    public void setGeS3(double geS3) {
        this.geS3 = geS3;
    }

    public double getGeS4() {
        return geS4;
    }

    public void setGeS4(double geS4) {
        this.geS4 = geS4;
    }
}
