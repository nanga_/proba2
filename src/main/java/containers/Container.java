package containers;

import domain.UserData;

import java.util.List;
import java.util.Scanner;

public class Container {
    static Scanner sc = new Scanner(System.in);   // dałam static bo tylko tak mi działa, ale nie rozumiem czemu tak musi być
    static DataContainer dataContainer = new DataContainer();   // tu tak samo


    public static void main(String[] args) {

        final double Dmin = 0.9;  // minimalna głębokość posadowienia
        final double B = 1.4;   // założony wymiar boku fundamentu
        final double L = 3.0;   // założony wymiar boku fundamentu

        final double beton = 25; // ciężar betonu
        final double zasypka = 21; // cieżar zasypki

        // założono brak wody w podłożu - duże uproszczenie, ale na tym etapie tak zostaje

        dodaj();
        System.out.println("Dodaj kolejną");
        dodaj();
        pokaz();
        getGestosc(dataContainer.profil);
        getFi(dataContainer.profil);


    }

    private static void dodaj() {
        UserData userData = new UserData();

        System.out.println("Podaj nazwe warstwy:");
        userData.setNazwaWarstwy1(getUserInput());

        System.out.println("Podaj ID/IL warstwy:");
        userData.setIw1(Double.valueOf(getUserInput()));

        System.out.println("Podaj miąższość warstwy:");
        userData.setGrw1(Double.valueOf(getUserInput()));

        dataContainer.dodajWarstwe(userData);

    }

    private static void pokaz() {

        System.out.println("Warstwy w profilu:");
        UserData userData;
        for (int i = 0; i < dataContainer.getProfil().size(); i++) {
            userData = dataContainer.getProfil().get(i);            // uwaga: getProfil zwraca Liste!!
            System.out.println(userData.getNazwaWarstwy1());
        }
    }

    public static void getGestosc(List<UserData> profil) {    // tu wcześniej była metoda find - zmieniłam nazwe
        // na razie zostaje void

        String[] niespoiste = {"zwir", "pospolka", "piasek"};   // tu wypisuje wszystkie  nazwy gruntow niespoiste - z normy
        String[] spoiste = {"glina", "pyl"};   // tu wypisuje wszystkie nazwy gruntow spoistych - z normy


        for (int i = 0; i < profil.size(); i++) {    // tu iteruje po obiektach listy profil - wprowadza je uzytkownik

            //String warstwa = dataContainer.profil.get(i).nazwaWarstwy1;
            String warstwa = dataContainer.profil.get(i).nazwaWarstwy1;
            double iw = dataContainer.profil.get(i).iw1;
            double[] ge = new double[profil.size()];  // nowa tablica do przechowywania gestości poszczególnych warstw

            if (warstwa.equals(niespoiste[0]) || warstwa.equals(niespoiste[1]) || warstwa.equals(niespoiste[2])) {   // tu moze by uzyć switch chyba byloby bardziej czytelne

                for (int j = 0; j < dataContainer.tworzListeGrNs().size(); j++) {  // azs!!! tu potrzebuje drugiej petli?
                    // zeby iterowala po wszystkich obiektach listy tworzListeGrNs

                    if (warstwa.equals(dataContainer.tworzListeGrNs().get(j).getNazwaGrNs())) {

                        if (iw <= 0.33) {   // dane z tablicy nr 1
                            ge[i] = dataContainer.tworzListeGrNs().get(j).getGe1();
                        } else if (iw > 0.33 && iw <= 0.67) {
                            ge[i] = dataContainer.tworzListeGrNs().get(j).getGe2();
                        } else {
                            ge[i] = dataContainer.tworzListeGrNs().get(j).getGe3();
                        }
                    }
                }
            } else if (warstwa.equals(spoiste[0]) || warstwa.equals(spoiste[1])) {

                for (int w = 0; w < dataContainer.tworzListeGrS().size(); w++) {// petla iteruje po wszystkich obiektach listy tworzListeGrS

                    if (warstwa.equals(dataContainer.tworzListeGrS().get(w).getNazwaGrS())) {

                        if (iw < 0) {   // dane z tablicy nr 2
                            ge[i] = dataContainer.tworzListeGrS().get(w).getGeS1();
                        } else if (iw >= 0 && iw <= 0.25) {
                            ge[i] = dataContainer.tworzListeGrS().get(w).getGeS2();
                        } else if (iw > 0.25 && iw <= 0.50) {
                            ge[i] = dataContainer.tworzListeGrS().get(w).getGeS3();
                        } else {
                            ge[i] = dataContainer.tworzListeGrS().get(w).getGeS4();
                        }
                    }
                }
            }
            System.out.println("Gęstość objętościowa warstwy nr: " + i + " " + ge[i] + " kg/m3");

        }

    }


    public static void getFi(List<UserData> profil) {    // dla każdej warstwy podanej przez uzytkownika trzeba fi obliczyć
        // wg: Rys 3(niespoiste) lub Rys 4 (spoiste)

        String[] niespoiste = {"zwir", "pospolka", "piasek"};   // wiem że duplikuje kot:P i że tak nie wolno - zmienie to - obiecuje:) nawet chyba już wiem jak:)
        String[] spoiste = {"glina", "pyl"};

        for (int i = 0; i < profil.size(); i++) {

            String warstwa = dataContainer.profil.get(i).nazwaWarstwy1;
            double iw = dataContainer.profil.get(i).iw1;
            double[] fi = new double[profil.size()];  // nowa tablica do przechowywania fi

            if (warstwa.equals(niespoiste[0]) || warstwa.equals(niespoiste[1])) {
                fi[i] = 1.44 * iw + 11.45;
            } else if (warstwa.equals(niespoiste[2])) {
                fi[i] = 10.45 * iw + 7.85;
            } else if (warstwa.equals(spoiste[0])) {
                fi[i] = 0.15 * iw + 1.33;
            } else {
                fi[i] = 8.18 * iw + 7.65;
            }

            System.out.println("Kąt tarcia: " + fi[i]);
        }
    }


    public static String getUserInput() {
        return sc.nextLine().trim();
    }



}
