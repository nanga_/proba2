package containers;

import domain.GruntyGeNs;
import domain.GruntyGeS;
import domain.UserData;

import java.util.ArrayList;
import java.util.List;

public class DataContainer {

    public List<UserData> profil = new ArrayList<>();

    public List<GruntyGeNs> listaGrNs;
    public List<GruntyGeS> listaGrS;

    public DataContainer() {
        listaGrNs = tworzListeGrNs();
        listaGrS = tworzListeGrS();
    }

    public void dodajWarstwe(UserData userData){
        profil.add(userData);

    }

    public List<UserData> getProfil(){
        return profil;
    }

    public List<GruntyGeNs>  tworzListeGrNs(){       // to sa tabularyczne dane z normy tablica nr 1

        List<GruntyGeNs> listaNs = new ArrayList<>();
        GruntyGeNs gruntNs1 = new GruntyGeNs("zwir", 11, 12, 13);
        listaNs.add(gruntNs1);
        GruntyGeNs gruntNs2 = new GruntyGeNs("pospolka", 14, 15, 16);
        listaNs.add(gruntNs2);
        GruntyGeNs gruntNs3 = new GruntyGeNs("piasek", 17, 18, 19);
        listaNs.add(gruntNs3);

        return listaNs;
    }

    public List<GruntyGeS> tworzListeGrS(){       // to sa tabularyczne dane z normy tablica nr 2

        List<GruntyGeS> listaS = new ArrayList<>();
        GruntyGeS gruntS1 = new GruntyGeS("glina", 11, 12, 13, 14);
        listaS.add(gruntS1);
        GruntyGeS gruntS2 = new GruntyGeS("pyl", 14, 15, 16, 16.5);
        listaS.add(gruntS2);

        return listaS;
    }
}
